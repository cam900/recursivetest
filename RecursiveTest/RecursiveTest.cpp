﻿
#include <iostream>
#include <vector>
#include <algorithm>

// 순열 구하기
template<class T>
void print_vector(const std::vector<T>& vec)
{
	for (auto e : vec)
	{
		std::cout << e << ", ";
	}
	std::cout << std::endl;
}

void permutation(std::vector<int>& vec, int k)
{
	// 기저조건
	if (k == (vec.size() - 1))
	{
		print_vector(vec);
		return;
	}
	// 재귀 호출
	for (int i = k; i < vec.size(); i++)
	{
		std::swap(vec[k], vec[i]);
		permutation(vec, k + 1);
		std::swap(vec[k], vec[i]); // 원래자리로 돌아오기
	}
}

int main()
{
	std::vector<int> vec{ 1,2,3,4 };
	permutation(vec, 0);
}
/*
// 최대공약수 구하기
int gcd(int a, int b)
{
	return (b == 0) ? a : gcd(b, a % b);
}

// gcd(24, 18) -> gcd(18, 6) -> gcd(6, 0) -> 6
// gcd(18, 24) -> gcd(24, 18) -> gcd(18, 6) -> gcd(6, 0) -> 6

// 최소공배수 구하기
int lcm(int a, int b)
{
	return a * b / gcd(a, b);
}

int main()
{
	std::cout << gcd(24, 18) << std::endl;
	std::cout << gcd(18, 24) << std::endl;
	std::cout << lcm(18, 24) << std::endl;
}
*/
/*
* // 문자열 뒤집기
std::string reverse(const std::string& str)
{
	return (str.length() == 0) ? "" : reverse(str.substr(1)) + str[0];
}

int main()
{
	std::string str = "HELLO";
	std::cout << reverse(str) << std::endl;
}
*/
/*
// n번째의 피보나치 수열 값 구하기
 // 1, (1+0)1, (1+1)2, (2+1)3, (3+2)5, (5+3)8, (8+5)13, (13+8)21, (21+13)34, (34+21)55, ...
long long fibo(long long n)
{
	return (n < 1) ? 0 : ((n <= 1) ? n : (fibo(n - 1) + fibo(n - 2)));
}
*/
/*
int main()
{*/
	/*
	for (long long i = 1; i <= 10; i++)
	{
		std::cout << fibo(i) << ", ";
	}
	std::cout << std::endl;
	*/
/*
	std::cout << fibo(48) << std::endl;
}
*/
/*
// n! 구하기 : 1부터 n까지의 곱을 반환하는 함수
long long factorial(long long n)
{
	return (n <= 1) ? 1 : (n * factorial(n - 1));
}

int main()
{
	std::cout << factorial(5) << std::endl;
	std::cout << factorial(10) << std::endl;
	std::cout << factorial(20) << std::endl;
}
*/
/*
// 1부터 n까지의 합을 반환하는 함수 - 반복문
int sum_iterative(int n)
{
	int s = 0;
	for (int i = 1; i <= n; i++)
	{
		s += i;
	}
	return s;
}

// 1부터 n까지의 합을 반환하는 함수 - 재귀호출
int sum_recursive(int n)
{
	// 기저조건 : 재귀호출을 종료
	// 재귀호출부 : 자기 자신을 호출, 매개변수는 규칙을 가지고 조금씩 변경
	return (n == 1) ? n : ((n < 1) ? 0 : (n + sum_recursive(n - 1)));
}

int main()
{
	//std::cout << sum_iterative(100) << std::endl;
	std::cout << sum_recursive(100) << std::endl;
}
*/